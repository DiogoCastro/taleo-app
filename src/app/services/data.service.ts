import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(public http:Http) {
    
  }

  getPost() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
    .map(res => res.json());
  }

  getPostById(id) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/' + id)
    .map(res => res.json());
  }

  editPost(id, post) {
    return this.http.put('https://jsonplaceholder.typicode.com/posts/' + id, JSON.stringify(post))
    .map(res => res.json());
  }

  deletePost(id) {
    return this.http.delete('https://jsonplaceholder.typicode.com/posts/' + id)
    .map(res => res.json());
  }

}
