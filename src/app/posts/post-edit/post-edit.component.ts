import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css', '../../../assets/materialize.min.css']
})

export class PostEditComponent {
  post:any;

  constructor(private route:ActivatedRoute, private router:Router, public dataService:DataService) {
      this.route.params.subscribe((params:Params) => {
        this.dataService.getPostById(params.id).subscribe(post => {
          this.post = post;
        });
        //console.log(params);
      });
  }

  ngOnInit() {
  }

  onSubmit(id) {
    this.dataService.editPost(1,this.post).subscribe(res => {
      console.log('PUT');
    })
  }


}
